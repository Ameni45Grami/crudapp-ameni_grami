const fs = require("fs");
const projects = JSON.parse(fs.readFileSync(`${__dirname}/../Data/data.json`));

exports.getprojectswithtasks = (req, res) => {
  const projectstasks = projects.filter((el) => el.hasOwnProperty("Tasks"));

  res.status(200).json({
    status: "success",
    results: projects.length,
    data: {
      projectstasks,
    },
  });
};

exports.getprojectswithouttasks = (req, res) => {
  const projectswithouttasks = projects.filter(
    !((el) => el.hasOwnProperty("Tasks"))
  );
  res.status(200).json({
    status: "success",
    results: projects.length,
    data: {
      projects: projectswithouttasks,
    },
  });
};

exports.getproject = (req, res) => {
  const project = projects.findIndex((el) => el.hasOwnProperty("Tasks"));
  res.status(200).json({
    status: "success",
    data: {
      project: projects[project],
    },
  });
};

exports.deleteproject = (req, res) => {
  const project = projects.findIndex((el) => el.Status === "to do");
  delete projects[project];
  res.status(204).json({
    status: "success",
    data: {
      tour: "object was deleted successfully",
    },
  });
};

exports.createproject = (req, res) => {
  const newId = projects[projects.length - 1].id + 1;
  const newproject = Object.assign({ id: newId }, req.body);
  projects.push(newproject);
  fs.writeFile(
    `${__dirname}/../Data/data.json`,
    JSON.stringify(projects),
    (err) => {
      res.status(201).json({
        status: "success",
        data: {
          project: newproject,
        },
      });
    }
  );
};

exports.updateproject = (req, res) => {
  const id = req.params.id * 1;
  const project = projects.findIndex((el) => el.id === id);
  projects[project] = { ...projects[project], ...req.body };
  res.status(200).json({
    status: "success",
    data: {
      project: projects[project],
    },
  });
};
