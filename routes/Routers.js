const express = require('express');
const controller= require('../Controllers/controller');
const router = express.Router();
router.route('/withouttasks').get(controller.getprojectswithouttasks);
router.route('/withtasks').get(controller.getprojectswithtasks);
router.route('/oneproject').get(controller.getproject);
router.route('/deleteproject').delete(controller.deleteproject);
router.route('/withouttasks').post(controller.createproject);
router.route('/withtasks/:id').get(controller.getprojectswithtasks).patch(controller.updateproject)
module.exports = router;