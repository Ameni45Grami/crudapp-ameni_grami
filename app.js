const express = require("express");
const Router = require("./routes/Routers.js");

const app = express();
app.use(express.json());
app.use((req, res, next) => {
  console.log("Hello from the middleware 👋");
  next();
});


app.use("/api/v1/projects", Router);

module.exports = app;
